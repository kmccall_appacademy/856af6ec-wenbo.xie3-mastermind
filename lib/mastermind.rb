class Code
  PEGS = {
    "R" => :red,
    "G" => :green,
    "B" => :blue,
    "Y" => :yellow,
    "O" => :orange,
    "P" => :purple
  }

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string)
    pegs_arr = string.upcase.chars

    unless pegs_arr.all? { |char| PEGS.has_key?(char) }
      raise "Invalid colors!"
    end

    Code.new(pegs_arr)
  end

  def self.random
    random_pegs = (0..3).map { |i| PEGS.keys.sample }

    Code.new(random_pegs)
  end

  def [](i)
    pegs[i]
  end

  def exact_matches(other_code)
    exact_arr = pegs.select.with_index do |peg, i|
      peg == other_code[i]
    end

    exact_arr.size
  end

  def near_matches(other_code)
    near_arr = pegs.select.with_index do |peg, i|
      other_code.pegs.include?(peg)
    end

    near_arr.uniq.size - exact_matches(other_code)
  end

  def ==(other_code)
    return false unless other_code.is_a?(Code)

    pegs == other_code.pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code=Code.random)
    @secret_code = secret_code
  end

  def get_guess
    print "Guess the code: "
    guess = gets.chomp

    Code.parse(guess)
  end

  def display_matches(guess)
    print "#{secret_code.exact_matches(guess)} exact matches."
    print "#{secret_code.near_matches(guess)} near matches."
  end
end
